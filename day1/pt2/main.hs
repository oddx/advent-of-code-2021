main :: IO ()
main = do
   input <- readFile "input.txt"
   let ints = map read (lines input) :: [Int]
   print . length . filter (== "Increased") $ parse ints []

delta :: Int -> Int -> String
delta x y
   | x > y  = "Decreased"
   | x < y  = "Increased"
   | x == y = "No Change"

uLimit :: [Int] -> [Int]
uLimit xs = take 3 xs

lLimit :: [Int] -> [Int]
lLimit xs = drop 1 (take 4 xs)

foldLimit :: [Int] -> Int
foldLimit xs = foldl (+) 0 xs

spreadDelta :: [Int] -> [String]
spreadDelta xs = [delta (foldLimit . uLimit $ xs) (foldLimit . lLimit $ xs)]

parse :: [Int] -> [String] -> [String]
parse set@(_:xs) [] = parse xs ([] ++ spreadDelta xs)
parse set@(_:xs) ss = parse xs (ss ++ spreadDelta xs)
parse _ ss          = ss
