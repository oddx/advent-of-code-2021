main :: IO ()
main = do
   input <- readFile "input.txt"
   let ints = map read (lines input) :: [Int]
   print . length . filter (== "Increased") $ parse ints []

delta :: Int -> Int -> String
delta x y
   | x > y = "Decreased"
   | x < y = "Increased"

parse :: [Int] -> [String] -> [String]
parse (x:rest@(y:xs)) [] = parse rest ([] ++ [delta x y])
parse (x:rest@(y:xs)) ss = parse rest (ss ++ [delta x y])
parse _ ss               = ss
